# GITIL : Gitlab Issues Time In Lane

**GITIL** is a simple open-source Chrome extension that will indicate the amount of days an issue stayed in the same lane in [Gitlab](https://gitlab.com/)'s Kanban boards.


## Usage


### Installing 

At this time, the extension is not published yet on the [Chrome Web Store](https://chrome.google.com/webstore).
The only way to use it is to **[clone the repository](https://gitlab.com/jlengrand/gitlab-issues-time-in-lane)** and [load it as unpacked extension](https://stackoverflow.com/questions/24577024/install-chrome-extension-not-in-the-store) in Chrome. 

### Getting your personal access token.

To use the extension, you need to create [an access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), with **api** rights.
 
 Then, saved it in the extension's pop-up on the top right of your screen : 

 ![inserting access token](docs/submit-at.png)

 Once it is known, you should see the message *Access token exists* when you open the pop-up again. 

 ![existing access token](docs/known-at.png)


### Using the extension on Gitlab.com

The extension will automatically trigger when you reach a Gitlab issue board. Nothing else to do!

### Using the extension on your own Gitlab instance

To use the extension on another Gitlab instance, you will need to modify a few files. 

* The *matches* string from the **manifest.json** file.
* The *GITLAB_URL* constant in the **gitlab.js** file.

Reload the extension, and everything should work fine!


## Author

[Julien Lengrand-Lambert](https://github.com/jlengrand). You can contact me on [Twitter](https://twitter.com/jlengrand).

## Contributing

Ideas and Merge Requests are more than welcome! **[You can find a list of issues and suggestions here](https://gitlab.com/jlengrand/gitlab-issues-time-in-lane/issues)**

## LICENSE

See LICENSE for full text, but the license is [Creative Commons Attribution NonCommercial ShareAlike (CC-NC-SA)](https://tldrlegal.com/license/creative-commons-attribution-noncommercial-sharealike-(cc-nc-sa)).