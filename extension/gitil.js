import * as settings from './settings';
import * as gitlab from './gitlab';

console.log("Loaded GITIL!");

settings.setDefaultSettings();

// TODO: add spinner everywhere first

const issueQueue = [];
let projectId;
const observer = new MutationObserver((mutationsList, observer) => {
    if(!projectId) projectId = gitlab.findProjectId();

    mutationsList.map(m => {
        Array.from(m.addedNodes)
            .filter(c => c.classList && c.classList.contains("board-card"))
            .map(el=> {
                issueQueue.push(el);
            });
    });
});

observer.observe(
    document.getElementsByClassName("boards-list")[0], 
    { attributes: false, childList: true, subtree: true });

function processIssue(issueElement, projectId, accessToken){
    const iid = gitlab.getIidFromIssueElement(issueElement);
    console.log(`Processing issue ${iid}`);
    gitlab.getEventsForIssue(projectId, iid, accessToken)
    .then(res => {
        if (res.status !== 200) throw(`Problem getting events. Status Code: ${res.status}`);

        res.json()
        .then(events => {
            const addEvents = gitlab.getAddEvents(events);

            if(addEvents.length == 0){
                gitlab.getIssue(projectId, iid, accessToken)
                .then(issueRes => {
                    if (issueRes.status !== 200) throw(`Problem getting events. Status Code: ${issueRes.status}`);
                    issueRes.json()
                    .then(issue => {
                        const timeInLane = gitlab.findTimeInLaneFromIssue(issue);
                        gitlab.insertTimeInIssue(timeInLane, issueElement);
                    })
                })
            }
            else{
                const timeInLane = gitlab.findTimeInLaneFromEvents(addEvents);
                gitlab.insertTimeInIssue(timeInLane, issueElement);
            }
        });
    })
    .catch(err => {
        // Back in the queue
        issueQueue.push(issueElement)
        console.log(err);
    });
}

settings.getAccessToken()
    .then(accessToken => {
        setInterval(() => { 
            const issueElement = issueQueue.shift();
            if(issueElement){
                processIssue(issueElement, projectId, accessToken);
            }
            else{
                // console.log("No issue to process in queue");
                //TODO : If happens for a while, go to sleep until new mutation events
            }
        }, 100);
    })
    .catch(err => console.log(`access token not known. Not doing anything : ${err}`));
