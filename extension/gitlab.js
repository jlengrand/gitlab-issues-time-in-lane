import moment from '../node_modules/moment/src/moment';

import * as settings from './settings';

export const GITLAB_URL = findRootGitlabUrl();

function findRootGitlabUrl(){
    return window.location.origin;
}

export function getIidFromIssueElement(issueElement){
    const link = issueElement.querySelector("a").href;
    return link.split("/").pop();
}

export function findProjectId(){
    return document.getElementById("filtered-search-boards").dataset.projectId;
}

export function getEventsForIssue(projectId, issueIid, accessToken){
    const url = new URL(`${GITLAB_URL}/api/v4/projects/${projectId}/issues/${issueIid}/resource_label_events?private_token=${accessToken}&per_page=100`)
    return fetch(url);
}

export function getIssue(projectId, issueId, accessToken){
    let url = new URL(`${GITLAB_URL}/api/v4/projects/${projectId}/issues/${issueId}`)
    const params = {private_token:accessToken}
    url.search = new URLSearchParams(params)
    return fetch(url);
}

export function findTimeInLaneFromIssue(issue){
    return moment().diff(moment(issue.created_at), 'days');
}

export function findTimeInLaneFromEvents(addEvents){
    const sortedAddEvents = sortEventsArrayByCreatedAt(addEvents);
    if(sortedAddEvents.length == 0) return "?"; 
    return moment().diff(moment(sortedAddEvents[0].created_at), 'days');
}

export function getAddEvents(events){
    return events.filter((item => item.action.localeCompare('add') == 0));
}

export function insertTimeInIssue(timeInLane, issueElement){
    const footer = issueElement.getElementsByClassName("board-card-footer")[0];

    const span = document.createElement("span");
    span.textContent = `${timeInLane} days`;
    if(timeInLane > settings.MAX_DAYS){
        span.style.color = "red";
    }

    footer.appendChild(span);
}

// Gitlab returns already filtered elements?
function sortEventsArrayByCreatedAt(events){
    return events.sort((a, b) => {
        const keyA = new Date(a.created_at);
        const keyB = new Date(b.created_at);
        if(keyA < keyB) return 1;
        if(keyA > keyB) return -1;
        return 0;
    });
}