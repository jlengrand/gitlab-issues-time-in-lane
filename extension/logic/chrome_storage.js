function getAccessToken() {
    return new Promise(function(resolve, reject){
        chrome.storage.sync.get(["accessToken"], (result) => {
            result.accessToken === undefined ? reject() : resolve(result.accessToken);
        });
    });
}