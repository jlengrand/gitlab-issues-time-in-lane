const LABELS_IGNORE = ["Open", "Closed"];
const MAX_DAYS = 2;

function setDefaultSettings(){
    console.log('Setting default settings');
    // Settings default settings if not found
    chrome.storage.sync.get(["maxDays", "labelsToIgnore"], (result) => {
        if(result.maxDays === undefined){
            console.log("Setting maxDays setting");
            chrome.storage.sync.set({maxDays : MAX_DAYS} , () => {});
        }

        if(result.labelsToIgnore === undefined){
            console.log("Setting labelsToIgnore setting");
            chrome.storage.sync.set({labelsToIgnore : LABELS_IGNORE} , () => {});
        }
    });
}

function resetDefaultSettings(){
    console.log('Resetting to default settings');
    chrome.storage.sync.set({maxDays : MAX_DAYS} , () => {});
    chrome.storage.sync.set({labelsToIgnore : LABELS_IGNORE} , () => {});
}

