export const MAX_DAYS = 2;

export function setDefaultSettings(){
    console.log('Setting default settings if they don\'t exist');
    // Settings default settings if not found
    chrome.storage.sync.get(["maxDays"], (result) => {
        if(result.maxDays === undefined){
            console.log("Setting maxDays setting");
            chrome.storage.sync.set({maxDays : MAX_DAYS} , () => {});
        }
    });
}

export function resetDefaultSettings(){
    console.log('Back to default settings');
    chrome.storage.sync.set({maxDays : MAX_DAYS} , () => {});
}

export function getAccessToken() {
    return new Promise(function(resolve, reject){
        chrome.storage.sync.get(["accessToken"], (result) => {
            result.accessToken === undefined ? reject() : resolve(result.accessToken);
        });
    });
}