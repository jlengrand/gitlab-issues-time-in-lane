import '@vaadin/vaadin-button';
import '@vaadin/vaadin-text-field/vaadin-number-field';
import '@vaadin/vaadin-text-field/vaadin-password-field';

import * as settings from './settings';

function updateView(){
    chrome.storage.sync.get(["accessToken", "maxDays"], (result) => {
        if(result.accessToken !== undefined){
            document.getElementById("accessToken").value = result.accessToken;
        }

        if(result.maxDays !== undefined){
            document.getElementById("maxDays").value = result.maxDays;
        }
    });
}

function saveSettings(){
    const accessTokenElement = document.getElementById("accessToken");
    const maxDaysElement = document.getElementById("maxDays");

    if(hasChanged(accessTokenElement)){
        chrome.storage.sync.set({accessToken : accessTokenElement.value} , () => {});
    }

    if(hasChanged(maxDaysElement)){
        chrome.storage.sync.set({maxDays : maxDaysElement.value} , () => {});
    }
}

function hasChanged(setting){
    return (setting && setting.value.length > 0);
}

settings.setDefaultSettings();
updateView();

window.addEventListener('DOMContentLoaded', event =>{
    const loginButton = document.getElementById('saveSettingsButton');
    loginButton.addEventListener('click', () => { saveSettings(); updateView();});
});

window.addEventListener('DOMContentLoaded', event =>{
    const resetButton = document.getElementById('resetButton');
    resetButton.addEventListener('click', () => { settings.resetDefaultSettings(); updateView();});
});