import resolve from 'rollup-plugin-node-resolve';

export default [{
    input: 'extension/ui.js',
    output: {
        dir: 'extension/dist/',
        format: 'esm'
    },
    plugins: [ resolve() ]
},
{
    input: 'extension/gitil.js',
    output: {
        dir: 'extension/dist',
        format: 'esm'
    },
    plugins: [ resolve() ]
},
];

